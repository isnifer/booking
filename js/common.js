(function ($) {

    /*
    * Add cities depending on Array.length and rows calculation
    *
    * @param {Array} arr Array with data of cities
    * @param {Number} num Array length
    * @param {Number} rows Number of rows in lists
    * @param {Object} list DOM Node for formatted data
    * @param {Object} content DOM Node for formatted data
    *
    * */
    function addCities (arr, num, rows, list, content) {

        var i = 0,
            first = document.createElement('ul'),
            second = document.createElement('ul'),
            third = document.createElement('ul');

        first.className = 'b-cities__list';
        second.className = 'b-cities__list';
        third.className = 'b-cities__list';

        for (i; i < num; i++) {

            var citiesLi = document.createElement('li'),
                citiesSpan = document.createElement('span'),
                contentDiv = document.createElement('div');

            // Create clickable item of city
            citiesLi.className = 'b-cities__item';
            citiesLi.setAttribute('data-id', 'city_' + i);
            citiesSpan.className = 'b-cities__name';

            citiesSpan.innerHTML = arr[i].name + ' (' + arr[i].count + ')';
            citiesLi.appendChild(citiesSpan);

            // Create content item
            contentDiv.className = 'b-content__item';
            contentDiv.setAttribute('id', 'city_' + i);
            contentDiv.innerHTML = arr[i].content;

            // Add content item to page
            content.appendChild(contentDiv);

            // Switch conditions (then num > 4 || then num === 4 || then num < 4, )
            if (num > 4 && i < rows || num === 4 && i <= 1 || num < 4 && i === 0) {
                first.appendChild(citiesLi);
            } else if (num > 4 && i >= rows && i < (rows * 2) || num === 4 && i === 2 || num < 4 && i === 1) {
                second.appendChild(citiesLi);
            } else if (num > 4 && i >= (rows * 2) || num === 4 && i === 3 || num < 4 && i === 2) {
                third.appendChild(citiesLi);
            }

        }

        // Add lists to page
        list.appendChild(first);
        list.appendChild(second);
        list.appendChild(third);

    }

    /*
     * Rows calculation
     *
     * @param {Array} arr Array with data of cities
     * @param {Object} list DOM Node for formatted data
     * @param {Object} content DOM Node for formatted data
     *
     * */
    function setRows (arr, list, content) {

        if (typeof arr.push === 'function') {

            var num = arr.length,
                rows = 0;

            // Make a calculation - how many rows will be in the list
            if (num > 4 && num % 3 === 0) {

                rows = Math.floor(num / 3);

            } else if (num > 4 && num % 3 !== 0) {

                rows = Math.floor(num / 3) + 1;

            } else if (num === 4) {

                rows = 2;

            }

            addCities(arr, num, rows, list, content);

        }

        return false;

    }

    // Load fresh data about cities
    $.ajax({
        url: 'cities.json',
        method: 'get',
        dataType: 'json',
        success: function (data) {

            var $content = $('.b-content')[0],
                $cities = $('.b-cities')[0],

                /*
                * Sort an array in alphabetical order
                *
                * @returns Array
                * */
                sortedCities = data.sort(function(field1, field2) {
                    var city1 = field1['name'], city2 = field2['name'];
                    return (city1 > city2) ? 1 : ( (city2 > city1) ? -1 : 0 );
                });

            setRows(sortedCities, $cities, $content);

        },
        error: function (error) {
            console.log(error);
            var $content = document.getElementsByClassName('b-content')[0];
            $content.innerHTML = 'Cannot load data of cities.';
        }
    });

    // Handler for view content about city
    $('body').on('click', '.b-cities__name', function (e) {
        e.preventDefault();

        var $this = $(this),
            currentId = $this.parent().attr('data-id'),
            activeContentClassName = 'b-content__item_state_visible',
            activeCitiesClassName = 'b-cities__name_state_active';

        if ( $('#' + currentId).length ) {

            (function () {

                $('.' + activeContentClassName).removeClass(activeContentClassName);
                $('.' + activeCitiesClassName).removeClass(activeCitiesClassName);
                $this.addClass(activeCitiesClassName);
                $('#' + currentId).addClass(activeContentClassName);

            }());

        }

    });

}(jQuery));